from py_omaha import py_omaha
import unittest
import requests
#from . import config
import config

class TestLogin(unittest.TestCase):
    def connection(self):
        return py_omaha.Omaha(config.TEST_URL, config.TEST_USERNAME, config.TEST_PASSWORD)

    def test_login(self):
        connection = self.connection()
        self.assertTrue(connection.login())
        self.assertTrue(connection.is_logged_in())

    def test_is_logged_in(self):
        connection = self.connection()
        self.assertFalse(connection.is_logged_in())

    def test_login_fail_wrong_password(self):
        connection= py_omaha.Omaha(config.TEST_URL, config.TEST_USERNAME, "wrong_password")
        self.assertFalse(connection.login())

    def test_login_fail_wrong_host(self):
        connection = py_omaha.Omaha("https://192.168.180.1:8043/", "api", "wrong_password")
        with self.assertRaises(Exception):
            connection.login()

class TestDevices(unittest.TestCase):
    def setUp(self):
        self.connection = py_omaha.Omaha(config.TEST_URL, config.TEST_USERNAME, config.TEST_PASSWORD)
        self.connection.login()

    def test_get_devices(self):
        result = self.connection.get_devices()
        self.assertIsInstance(result, dict)
        self.assertEqual(result["errorCode"], 0)
        self.assertIsInstance(result["result"], list)

    def test_patch_device(self):
        device = config.TEST_DEVICE
        data = {"radioSetting5g":{"radioEnable":True}}
        result = self.connection.patch_device(device, data)
        self.assertIsInstance(result, dict)

if __name__ == '__main__':
    unittest.main()